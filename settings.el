(setq user-full-name "Pathompong Kwangtong")
(setq user-mail-address "foxmean@protonmail.com")

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(package-initialize)

;; This is only needed once, near the top of the file
(eval-when-compile
  ;; Following line is not needed if use-package.el is in ~/.emacs.d
  (add-to-list 'load-path "<path where use-package is installed>")
  (require 'use-package))

(add-to-list 'load-path "~/.emacs.d/lisp/")

(use-package auto-package-update
  :ensure t
  :init
  :config
  (setq auto-package-update-delete-old-versions t)
  (setq auto-package-update-hide-results t)
  (auto-package-update-maybe))

(use-package which-key 
  :ensure t
  :init
  :config
  (which-key-mode 1))

(use-package ivy
  :ensure t
  :init
  (setq vy-use-virtual-buffers t)
  :config
  (ivy-mode 1)
  (setq enable-recursive-minibuffers t))

(use-package projectile
  :ensure t
  :init
  :config
  (projectile-mode 1)
  :bind (("s-p" . projectile-command-map)
         ("C-c p" . projectile-command-map)))

(use-package ace-window
  :ensure t
  :init)

(global-set-key (kbd "C-x o") 'ace-window)

(setq make-backup-files nil)
(setq auto-save-default nil)

(use-package magit
  :ensure t
  :init)

(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "C-x M-g") 'magit-dispatch)

(use-package git-timemachine
  :ensure t
  :init)

(use-package diff-hl
  :ensure t
  :init
  :config
  (global-diff-hl-mode 1))
(add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)

(setq make-backup-files nil)

(use-package emms
  :ensure t
  :init)
(add-to-list 'emms-player-list 'emms-player-mpv)

(use-package pdf-tools
  :ensure t
  :init
  :magic ("%PDF" . pdf-view-mode)
  :config
  (pdf-tools-install :no-query))

(use-package use-package
  :ensure t
  :init
  (setq nov-text-width 80))
(defun my-nov-font-setup ()
  (face-remap-add-relative 'variable-pitch :family "Liberation Serif"
                                           :height 1.0))
(add-hook 'nov-mode-hook 'my-nov-font-setup)
(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))

(add-to-list 'auto-mode-alist '("\\.\\(cbr\\)\\'" . archive-mode))

(global-set-key (kbd "C-c c") 'clone-indirect-buffer-other-window)

(use-package flycheck
  :ensure t
  :init
  :config
  (global-flycheck-mode 1))
(add-hook 'haskell-mode-hook #'flycheck-haskell-setup)
(use-package flycheck-ledger
  :ensure t
  :init)

(use-package company
  :ensure t
  :init)
(add-hook 'after-init-hook 'global-company-mode)

(use-package yasnippet
  :ensure t
  :init
  :config
  (yas-global-mode 1))

(use-package yasnippet-snippets
  :ensure t
  :init)

(use-package deft
  :ensure t
  :custom
    (deft-extensions '("org" "md" "txt"))
    (deft-directory "~/notes")
    (deft-use-filename-as-title t))

(use-package zetteldeft
  :ensure t
  :after deft
  :config 
   (zetteldeft-set-classic-keybindings)
   (visual-line-mode 1))

(use-package writeroom-mode
  :ensure t
  :init)

(use-package ledger-mode
  :ensure t
  :init
  (setq ledger-clear-whole-transactions 1))
(add-to-list 'auto-mode-alist '("\\.dat\\'" . ledger-mode))

(use-package slime
  :ensure t
  :init)

(use-package org
  :ensure t
  :init)
(use-package org-bullets
  :ensure t
  :init)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

(setq org-confirm-babel-evaluate nil)

(setq org-src-fontify-natively t)

(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c r") 'org-capture)
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c L") 'org-insert-link-global)
(global-set-key (kbd "C-c O") 'org-open-at-point-global)
(global-set-key (kbd "<f9> <f9>") 'org-agenda-list)
(global-set-key (kbd "<f9> <f8>") (lambda () (interactive) (org-capture nil "r")))

(display-time-mode 1)
(global-visual-line-mode 1)
(setq frame-title-format (concat "Foxmean's Emacs"))
(use-package emojify
  :ensure t
  :init)
(global-emojify-mode)
(global-prettify-symbols-mode 1) ; Display “lambda” as “λ”
(global-set-key (kbd "C-x C-l") 'display-line-numbers-mode)
(menu-bar-mode 1)
(tool-bar-mode 0)
(scroll-bar-mode 0)
(setq inhibit-startup-screen t)
(global-subword-mode 1) ; Move by SubWord
(show-paren-mode 1) ; Turn on bracket match highlight
(save-place-mode 1) ; Remember cursor position
(defalias 'yes-or-no-p 'y-or-n-p)
(use-package all-the-icons
  :ensure t
  :init)

(use-package spaceline
  :ensure t
  :init
  :config
  (spaceline-emacs-theme 1))
(use-package anzu
  :ensure t
  :init
  (setq anzu-cons-mode-line-p nil)
  :config)
(global-anzu-mode +1)

(use-package monokai-theme
  :ensure t
  :init
  :config
  (load-theme 'monokai 1))
